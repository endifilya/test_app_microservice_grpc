from test_pb2 import HellouResponse, GetDataResponse

import test_pb2_grpc
import grpc
from concurrent import futures

test_date = {
    5555:  { 'name': 'tetris' }
}

class GetDataService( test_pb2_grpc.GetDataServicer ):
    def getData(self, request, context):
        if request.number == '':
            context.abort(grpc.StatusCode.NOT_FOUND, "Empty")

        return GetDataResponse(name=test_date[request.number]['name'])

class HellouService( test_pb2_grpc.HelloServicer ):
    def Hi(self, request, context):
        if request.text == '':
            context.abort(grpc.StatusCode.NOT_FOUND, "Empty")

        return HellouResponse(text='test')

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    test_pb2_grpc.add_HelloServicer_to_server( HellouService(), server )
    test_pb2_grpc.add_GetDataServicer_to_server( GetDataService(), server )
    server.add_insecure_port("[::]:50053")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    serve()

