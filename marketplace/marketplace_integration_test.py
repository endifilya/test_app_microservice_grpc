from urllib.request import urlopen

def test_render_homepage():
    homepage_html = urlopen("http://localhost:5000").read().decode("utf-8")
    assert "<title>Online Books For You</title>" in homepage_html
    assert homepage_html.count("<li>") == 3

def test_render_test():
    homepage_html = urlopen("http://localhost:5000/test").read().decode("utf-8")
    assert "test" in homepage_html
    

def test_render_name():
    homepage_html = urlopen("http://localhost:5000/name").read().decode("utf-8")
    assert "tetris" in homepage_html
    