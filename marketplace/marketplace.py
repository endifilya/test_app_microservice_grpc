# marketplace/marketplace.py

import os

from flask import Flask, render_template
import grpc


from recommendations_pb2 import BookCategory, RecommendationRequest, GetNameRequest
from recommendations_pb2_grpc import RecommendationsStub

from test_pb2 import HellouRequest
from test_pb2_grpc import HelloStub

app = Flask(__name__)

recommendations_host = os.getenv("RECOMMENDATIONS_HOST", "localhost")
recommendations_channel = grpc.insecure_channel(
    f"{recommendations_host}:50051"
)

test_host = os.getenv("TEST_HOST", "localhost")
test_channel = grpc.insecure_channel(
    f"{test_host}:50053"
)

hello_client = HelloStub(test_channel)
recommendations_client = RecommendationsStub(recommendations_channel)



@app.route("/")
def render_homepage():
    recommendations_request = RecommendationRequest(
        user_id=1, category=BookCategory.MYSTERY, max_results=3
    )
    recommendations_response = recommendations_client.Recommend(
        recommendations_request
    )
    return render_template(
        "homepage.html",
        recommendations=recommendations_response.recommendations,
    )

@app.route("/test")
def test():
    hello_request = HellouRequest(
        text=123
    )
    hello_response = hello_client.Hi(
        hello_request
    )
    return hello_response.text

@app.route("/name")
def get_name():
    get_name_request = GetNameRequest(
        number=5555
    )
    get_name_response = recommendations_client.Name(
        get_name_request
    )
    return get_name_response.name